﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Threading.Tasks;
using System.Threading;
using Organa.Utils;
using System.Text.RegularExpressions;
using Microsoft.WindowsAPICodePack.Shell;

namespace Organa
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public class TreeItem: Utils.NotifyPropertyChanged
        {
            const FileAttributes ignore = FileAttributes.ReparsePoint | FileAttributes.Hidden;
            
            DirectoryInfo bdi;
            string name;
            bool onlyName = false;
            bool selected;
            bool opened;

            public TreeItem(DirectoryInfo di)
            {
                this.bdi = di;
                if (autoSelect)
                {
                    if (null != bdi && lastDir.Contains(bdi.FullName) && lastDir != bdi.FullName) IsOpen = true;
                    if (null != bdi && lastDir == bdi.FullName) IsSelect = true;
                }
            }

            public TreeItem(string name)
            {
                this.bdi = null;
                this.name = name;
            }

            public TreeItem(DirectoryInfo di, string name, bool onlyName=false):this(di)
            {
                this.name = name;
                this.onlyName = onlyName;
            }

            public void Refresh()
            {
                OnPropertyChanged("Items");
            }

            bool IsEnumerable(DirectoryInfo di)
            {
                return null != di && di.Exists && (di.Attributes & ignore) == 0;
            }

            public string Name
            {
                get 
                {
                    if (null != bdi && null != name && !onlyName) return name + ":" + bdi.Name;
                    else return (null != bdi && !onlyName) ? bdi.Name : name;
                }
            }

            public IEnumerable<TreeItem> Items
            {
                get
                {
                    try
                    {
                        return null!=bdi ? bdi.EnumerateDirectories().Where((d)=>IsEnumerable(d)).Select((d) => new TreeItem(d)) : null;
                    }
                    catch(Exception e)
                    {
                        return new List<TreeItem>() { new TreeItem(bdi.Attributes.ToString()+"; " + e.Message) };
                    }
                }
            }

            public bool IsSelect
            {
                get { return selected; }
                set { selected = value; if (value) Selected(this, bdi); OnPropertyChanged("IsSelect"); }
            }

            public bool IsOpen
            {
                get { return opened; }
                set { opened = value; OnPropertyChanged("IsOpen"); }
            }
        }

        public class File
        {
            FileInfo fi;

            public File(FileInfo fi)
            {
                this.fi = fi;
            }

            public string Name
            {
                get { return fi.Name; }
            }

            public string FullName
            {
                get { return fi.FullName; }
            }
        }

        public class OImage
        {
            string path;

            public OImage(string path)
            {
                this.path = path;
            }

            public string Path
            {
                get { return path; }
            }
        }

        ObservableCollection<TreeItem> tree = new ObservableCollection<TreeItem>();
        
        TreeItem cti;
        DirectoryInfo cdi;

        static MainWindow mw;
        static string lastDir;
        static bool autoSelect = true;

        const string cropDir = ".crop";
        bool isNewCrop;
        bool isCropOver;

        string[] filter = {"*.png"};
        static readonly Regex FileNameRegex = new Regex(@".*\d+.*", RegexOptions.Singleline | RegexOptions.Compiled);

        List<File> files;
        string titleAddition;
        string cropData;
        Int32Rect cropRect=new Int32Rect(-1,-1,0,0);

        ImageSource image;
        Task task;
        CancellationTokenSource cts;

        public MainWindow()
        {
            InitializeComponent();

            mw = this;
            DataContext = this;

            lastDir = Properties.Settings.Default.LastDir;

            if (ShellLibrary.IsPlatformSupported)
            {
                bool documents = false;
                foreach (ShellObject obj in ShellLibrary.LibrariesKnownFolder)
                {
                    try
                    {
                        ShellLibrary sl = ShellLibrary.Load(obj.Name, true);
                        if (obj.Name == "Documents") documents = true;
                        foreach (var f in sl)
                        {
                            if (f.IsFileSystemObject) tree.Add(new TreeItem(new DirectoryInfo(f.Path), obj.Name));
                        }
                        sl.Dispose();
                    }
                    catch { }
                }

                try
                {
                    if (!documents)
                    {
                        ShellLibrary sl = ShellLibrary.Load(KnownFolders.DocumentsLibrary, true);
                        foreach (var f in sl)
                        {
                            if (f.IsFileSystemObject && !f.IsLink) tree.Add(new TreeItem(new DirectoryInfo(f.Path), f.Name, true));
                        }
                        sl.Dispose();
                    }
                }catch{}

                try
                {
                    foreach (ShellLink obj in KnownFolders.Links)
                    {
                        try
                        {
                            var s = ShellFolder.FromParsingName(obj.TargetLocation) as ShellFolder;
                            if (s.IsFileSystemObject && !s.IsLink) tree.Add(new TreeItem(new DirectoryInfo(obj.TargetLocation), obj.Name, true));
                            s.Dispose();
                        }
                        catch { }
                        obj.Dispose();
                    }
                }
                catch { }
            }

            DriveInfo[] drives = DriveInfo.GetDrives();
            drives.Where((di) => di.IsReady).Select((di) => di.RootDirectory).ToList().ForEach((d) => tree.Add(new TreeItem(d)));

            this.Closed += (a, b) => { if (null != cdi) Properties.Settings.Default.LastDir = cdi.FullName; Properties.Settings.Default.Save(); };
        }

        static void Selected(TreeItem ti, DirectoryInfo di)
        {
            autoSelect = false;
            mw.List(ti, di);
        }

        void List(TreeItem ti, DirectoryInfo di)
        {
            cti = ti;
            cdi = di;
            if (null != cdi) files = filter.AsParallel().SelectMany(p =>
                cdi.EnumerateFiles(p).Where(fi=>FileNameRegex.IsMatch(fi.Name)).Select(fi => new File(fi))
                ).ToList();
            else files = null;

            OnPropertyChanged("Files");
            OnPropertyChanged("Images");

            Image = null;
            CropData = null;
            IsCropOver = false;
            IsNewCrop = false;
            TitleAddition = null;

            if (null != files && files.Count > 0)
            {
                BitmapImage srcOrig = Bmp.Load(files.Select(f => f.FullName).First());

                if (srcOrig.Format == PixelFormats.Bgra32)
                {
                    srcOrig.Freeze();

                    TitleAddition = string.Format("{0}x{1}x{2}", (int)srcOrig.Width, (int)srcOrig.Height, srcOrig.Format.BitsPerPixel);

                    var wbmpOrig = new WriteableBitmap((int)srcOrig.Width, (int)srcOrig.Height, 96, 96, PixelFormats.Bgra32, null);

                    Image = wbmpOrig;

                    uint pBackBufferOrig = (uint)wbmpOrig.BackBuffer;
                    int lengthOrig = wbmpOrig.PixelHeight * wbmpOrig.BackBufferStride;

                    int srcPixelWidth = wbmpOrig.PixelWidth;
                    int srcPixelHeight = wbmpOrig.PixelHeight;

                    if (null != cts) cts.Cancel();
                    cts = new CancellationTokenSource();

                    task = new Task(() =>
                    {
                        uint pBackBuffer = pBackBufferOrig;
                        int length = lengthOrig;
                        var wbmp = wbmpOrig;
                        BitmapImage src = srcOrig;
                        List<File> filesLocal = files;
                        var token = cts.Token;
                        int pixelWidth = srcPixelWidth;
                        int pixelHeight = srcPixelHeight;

                        int i = 0;
                        do
                        {
                            if (token.IsCancellationRequested) return;

                            ++i;

                            if (i == 1) Dispatcher.Invoke((Action)(() => wbmp.Lock()));

                            var wbmp0 = new WriteableBitmap(src);
                            uint pScrBuff = (uint)wbmp0.BackBuffer;

                            unsafe
                            {
                                for (int p = 0, s = 0; p < length; p += 4, s += wbmp0.Format.BitsPerPixel / 8)
                                {
                                    *((uint*)(pBackBuffer + p)) = Bmp.Alpha(*((uint*)(pScrBuff + s)), *((uint*)(pBackBuffer + p)));
                                }
                            }

                            if (i == 1) Dispatcher.Invoke((Action)(() =>
                               {
                                   wbmp.AddDirtyRect(new Int32Rect(0, 0, wbmp.PixelWidth, wbmp.PixelHeight));
                                   wbmp.Unlock();
                                   wbmp.Lock();
                               }));

                            while (i < filesLocal.Count)
                            {
                                src = Bmp.Load(filesLocal[i].FullName);
                                if (src.PixelWidth == pixelWidth || src.PixelHeight == pixelHeight) break;
                                ++i;
                            }

                        } while (i < filesLocal.Count);

                        Dispatcher.Invoke((Action)(() =>
                        {
                            wbmp.AddDirtyRect(new Int32Rect(0, 0, wbmp.PixelWidth, wbmp.PixelHeight));
                            wbmp.Unlock();
                        }));
                    }, cts.Token);

                    task.ContinueWith((t) =>
                    {
                        if (t == task)
                        {
                            Dispatcher.BeginInvoke((Action)(() =>
                            {
                                DetectCropMode(cdi.FullName);
                                MinRect(wbmpOrig);
                            }));
                        }
                    });

                    task.Start();
                }
            }
        }

        void MinRect(WriteableBitmap wbmp)
        {
            cropRect = Bmp.MinRect(wbmp);
            if (cropRect.X == -1)
            {
                IsNewCrop = false;
                IsCropOver = false;
                return;
            }

            Bmp.DrawRect(wbmp, cropRect);

            CropData = string.Format("{0}x{1}", cropRect.Width, cropRect.Height);
        }

        void DetectCropMode(string path)
        {
            bool newCrop = true;
            var cd = Path.Combine(path, cropDir);

            if (Directory.Exists(cd)) newCrop = FS.IsDirectoryEmplty(cd);

            IsNewCrop = newCrop;
            IsCropOver = !newCrop;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public ObservableCollection<TreeItem> Tree
        {
            get { return tree; }
        }

        public IEnumerable<File> Files
        {
            get { return files; }
        }

        public IEnumerable<OImage> Images
        {
            get 
            {
                return null != files ? files.Select((f) => new OImage(f.FullName)) : null; 
            }
        }

        public ImageSource Image
        {
            get {return image;}
            protected set { image = value;  OnPropertyChanged("Image"); }
        }

        public string TitleAddition
        {
            get
            {
                if (null != titleAddition) return "- " + titleAddition;
                else return "";
            }
            set { titleAddition = value; OnPropertyChanged("TitleAddition"); }
        }

        public string CropData
        {
            get { return cropData;}
            protected set { cropData=value; OnPropertyChanged("CropData"); }
        }

        public bool IsNewCrop
        {
            get { return isNewCrop; }
            protected set { isNewCrop = value; OnPropertyChanged("IsNewCrop"); }
        }

        public bool IsCropOver
        {
            get { return isCropOver;}
            protected set { isCropOver = value; OnPropertyChanged("IsCropOver");}
        }

        private void CropOver_Click(object sender, RoutedEventArgs e)
        {
            string cd = Path.Combine(cdi.FullName, cropDir);
            try
            {
                Directory.Delete(cd, true);
            }
            catch { }

            if(!Directory.Exists(cd) || FS.IsDirectoryEmplty(cd)) Crop_Click(sender, e);
        }

        private void Crop_Click(object sender, RoutedEventArgs e)
        {
            string cd=Path.Combine(cdi.FullName, cropDir);
            if(!Directory.Exists(cd)) Directory.CreateDirectory(cd);
            files.ForEach(f => 
            {
                Stream imageStreamSource = new FileStream(f.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
                PngBitmapDecoder decoder = new PngBitmapDecoder(imageStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                BitmapSource bitmapSource = decoder.Frames[0];

                FileStream stream = new FileStream(Path.Combine(cd,f.Name), FileMode.Create);
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Interlace = PngInterlaceOption.Off;
                encoder.Frames.Add(BitmapFrame.Create(new CroppedBitmap(bitmapSource, cropRect)));
                encoder.Save(stream);

                imageStreamSource.Close();
                stream.Close();
            });

            cti.Refresh();
        }
    }
}
