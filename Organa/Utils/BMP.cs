﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Organa.Utils
{
    public class Bmp
    {
        static public BitmapImage Load(string path)
        {
            BitmapImage src = new BitmapImage();
            src.BeginInit();
            src.UriSource = new Uri(path);
            src.CacheOption = BitmapCacheOption.OnLoad;
            src.EndInit();
            return src;
        }

        static public uint Alpha(uint from, uint to)
        {
            if ((to & 0xff000000) != 0 || (from & 0xff000000) != 0) return 0xff0000ff;
            else return 0;
        }

        static public uint Apply(uint from, uint to)
        {
            if ((to & 0xff000000) == 0xff000000) return to;
            else if (((from & 0xff000000) == 0xff000000)) return from;
            else return Avg(from, to);
        }

        static public uint Avg(uint from, uint to)
        {
            return Avg(from, to, 24) + Avg(from, to, 16) + Avg(from, to, 8) + Avg(from, to, 0);
        }

        static public uint Avg(uint f, uint t, int shift)
        {
            return ((((f >> shift) & 0xff + (t >> shift) & 0xff) / 2) & 0xff) << shift;
        }

        static public uint MaxA(uint from, uint to)
        {
            return (from & 0xff000000) > (to & 0xff000000) ? from : to;
        }

        static public Int32Rect MinRect(WriteableBitmap wbmp)
        {
            int y0 = 0;
            int y1 = wbmp.PixelHeight - 1;
            int yc0 = -1;
            int yc1 = -1;

            int x0 = 0;
            int x1 = wbmp.PixelWidth - 1;
            int xc0 = -1;
            int xc1 = -1;

            uint pBuff = (uint)wbmp.BackBuffer;

            while ((y0 < y1 || x0 < x1) && (yc0 == -1 || yc1 == -1 || xc0 == -1 || xc1 == -1))
            {
                int xy0 = xc0 < 0 ? x0 : xc0;
                int xy1 = xc1 < 0 ? x1 : xc1;

                if (yc0 < 0) if (Bmp.CheckH(y0, xy0, xy1, pBuff, wbmp.BackBufferStride)) yc0 = y0;
                if (yc1 < 0) if (Bmp.CheckH(y1, xy0, xy1, pBuff, wbmp.BackBufferStride)) yc1 = y1;

                int yx0 = yc0 < 0 ? y0 : yc0;
                int yx1 = yc1 < 0 ? y1 : yc1;

                if (xc0 < 0) if (Bmp.CheckV(x0, yx0, yx1, pBuff, wbmp.BackBufferStride)) xc0 = x0;
                if (xc1 < 0) if (Bmp.CheckV(x1, yx0, yx1, pBuff, wbmp.BackBufferStride)) xc1 = x1;

                if (y0 < y1) { if (yc0 < 0) ++y0; if (yc1 < 0) --y1; }
                if (x0 < x1) { if (xc0 < 0) ++x0; if (xc1 < 0) --x1; }
            }

            return new Int32Rect(xc0, yc0, xc1 - xc0 + 1, yc1 - yc0 + 1);
        }

        static public bool CheckH(int y, int x0, int x1, uint pBuff, int stride)
        {
            unsafe
            {
                for (; x0 <= x1; ++x0)
                    if ((*((uint*)(pBuff + y * stride + x0 * 4)) & 0xff000000) != 0) return true;
            }
            return false;
        }

        static public void LineH(int y, int x0, int x1, uint pBuff, int stride)
        {
            unsafe
            {
                for (; x0 <= x1; ++x0)
                {
                    *((uint*)(pBuff + y * stride + x0 * 4)) ^= 0x00ff0000;
                    *((uint*)(pBuff + y * stride + x0 * 4)) |= 0xff000000;
                }
            }
        }

        static public bool CheckV(int x, int y0, int y1, uint pBuff, int stride)
        {
            unsafe
            {
                for (; y0 <= y1; ++y0)
                    if ((*((uint*)(pBuff + y0 * stride + x * 4)) & 0xff000000) != 0) return true;
            }
            return false;
        }

        static public void LineV(int x, int y0, int y1, uint pBuff, int stride)
        {
            unsafe
            {
                for (; y0 <= y1; ++y0)
                {
                    *((uint*)(pBuff + y0 * stride + x * 4)) ^= 0x00ff0000;
                    *((uint*)(pBuff + y0 * stride + x * 4)) |= 0xff000000;
                }
            }
        }

        static public void DrawRect(WriteableBitmap wbmp, Int32Rect rect)
        {
            wbmp.Lock();
            uint pBuff = (uint)wbmp.BackBuffer;

            int x1 = rect.X + rect.Width - 1;
            int y1 = rect.Y + rect.Height - 1;

            Bmp.LineH(rect.Y, rect.X, x1, pBuff, wbmp.BackBufferStride);
            Bmp.LineH(y1, rect.X, x1, pBuff, wbmp.BackBufferStride);

            Bmp.LineV(rect.X, rect.Y, y1, pBuff, wbmp.BackBufferStride);
            Bmp.LineV(x1, rect.Y, y1, pBuff, wbmp.BackBufferStride);

            wbmp.AddDirtyRect(new Int32Rect(0, 0, wbmp.PixelWidth, wbmp.PixelHeight));
            wbmp.Unlock();
        }
    }

    // Thnx to: http://www.thejoyofcode.com/WPF_Image_element_locks_my_local_file.aspx
    public class UriToCachedImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return null;

            if (!string.IsNullOrEmpty(value.ToString()))
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new Uri(value.ToString());
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();
                return bi;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException("Two way conversion is not supported.");
        }
    }
}
