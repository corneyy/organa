﻿using System.Runtime.InteropServices;

namespace Organa.Utils
{
    public class FS
    {
        [DllImport("Shlwapi.dll", EntryPoint = "PathIsDirectoryEmpty")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsDirectoryEmplty([MarshalAs(UnmanagedType.LPStr)]string directory);
    }
}
